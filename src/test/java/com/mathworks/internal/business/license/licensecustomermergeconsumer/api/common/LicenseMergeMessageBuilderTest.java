package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.core.consumerFramework.ConsumerFailException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class LicenseMergeMessageBuilderTest {

  private ObjectMapper objectMapper = new ObjectMapper();
  private  LicenseMergeMessageBuilder builder = new LicenseMergeMessageBuilder(objectMapper);

  private static final String VALID_CUSTOMER_ID =  "C1234";
  private static final String EMPTY_CUSTOMER_ID =  "";
  private static final String NULL_CUSTOMER_ID =  null;
  @Test
  public void testMessage() throws Exception {
    String formattedInputJSON = String.format("{ \"sourceId\":\"%s\", \"sourceDupId\":\"%s\" }", VALID_CUSTOMER_ID, VALID_CUSTOMER_ID);
    MergeConsumerMessage message = builder.buildMessage(formattedInputJSON);
    Assert.assertEquals(VALID_CUSTOMER_ID, message.getNewId());
    Assert.assertEquals(VALID_CUSTOMER_ID, message.getOldId());
  }

  @Test(expected = ConsumerFailException.class)
  public void testMessageForMissingSourceId() throws Exception {
    String formattedInputJSON = String.format("{ \"sourceDupId\":\"%s\" }", VALID_CUSTOMER_ID);
    builder.buildMessage(formattedInputJSON);
  }

  @Test(expected = ConsumerFailException.class)
  public void testMessageForMissingSourceDupId() throws Exception {
    String formattedInputJSON = String.format("{ \"sourceId\":\"%s\" }", VALID_CUSTOMER_ID);
    builder.buildMessage(formattedInputJSON);
  }

  @Test(expected = ConsumerFailException.class)
  public void testMessageForEmptySourceId() throws Exception {
    String formattedInputJSON = String.format("{ \"sourceId\":\"%s\", \"sourceDupId\":\"%s\" }", EMPTY_CUSTOMER_ID, VALID_CUSTOMER_ID);
    builder.buildMessage(formattedInputJSON);
  }

  @Test(expected = ConsumerFailException.class)
  public void testMessageForEmptySourceDupId() throws Exception {
    String formattedInputJSON = String.format("{ \"sourceId\":\"%s\", \"sourceDupId\":\"%s\" }", VALID_CUSTOMER_ID, EMPTY_CUSTOMER_ID);
    builder.buildMessage(formattedInputJSON);
  }

  @Test(expected = ConsumerFailException.class)
  public void testMessageForNullSourceId() throws Exception {
    String formattedInputJSON = String.format("{ \"sourceId\":null, \"sourceDupId\":\"%s\" }", VALID_CUSTOMER_ID);
    builder.buildMessage(formattedInputJSON);
  }

  @Test(expected = ConsumerFailException.class)
  public void testMessageForNullSourceDupId() throws Exception {
    String formattedInputJSON = String.format("{ \"sourceId\":\"%s\", \"sourceDupId\":null }", VALID_CUSTOMER_ID);
    builder.buildMessage(formattedInputJSON);
  }
}
