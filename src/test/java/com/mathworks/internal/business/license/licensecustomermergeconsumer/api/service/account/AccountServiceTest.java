package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service.account;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service.LicenseeServiceTestHelper;
import com.mathworks.internal.business.license.masterlicensews.types.Audit;
import com.mathworks.internal.business.license.masterlicensews.types.MasterLicense;
import com.mathworks.internal.foundation.support.ws.spring.client.model.PageableList;
import com.mathworks.internal.foundation.support.ws.spring.client.rest.RestClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.util.MultiValueMap;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
  @Mock
  RestClient masterLicenseWSClientMock;
  @InjectMocks
  AccountService accountServiceMock;

  @Test
  public void testMergeWithoutAnyOffsets() throws Exception {
    MergeConsumerMessage message = LicenseeServiceTestHelper.getStubMessage();
    PageableList<MasterLicense> pageableList = LicenseeServiceTestHelper.getPageableListStub(1);
    pageableList.setReturnedRecords(LicenseeServiceTestHelper.getStubMasterLicenses());

    when(masterLicenseWSClientMock.getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class))).thenReturn(pageableList);
    when(masterLicenseWSClientMock.patch(any(), any(), any())).thenReturn(Collections.EMPTY_LIST);
    accountServiceMock.merge(message);
    verify(masterLicenseWSClientMock, times(1)).getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class));
    verify(masterLicenseWSClientMock, times(1)).patch(any(), any(), any());
  }

  @Test
  public void testMergeWithOffsets() throws Exception {
    MergeConsumerMessage message = LicenseeServiceTestHelper.getStubMessage();
    PageableList<MasterLicense> pageableList = LicenseeServiceTestHelper.getPageableListStub(30);
    pageableList.setReturnedRecords(LicenseeServiceTestHelper.getStubMasterLicenses());

    when(masterLicenseWSClientMock.getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class))).thenReturn(pageableList);
    when(masterLicenseWSClientMock.patch(any(), any(), any())).thenReturn(Collections.EMPTY_LIST);
    accountServiceMock.merge(message);
    verify(masterLicenseWSClientMock, times(2)).getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class));
    verify(masterLicenseWSClientMock, times(2)).patch(any(), any(), any());
  }

  @Test
  public void testMergeByOffsets() throws Exception {
    Audit audit = LicenseeServiceTestHelper.getStubbedAudit();
    MergeConsumerMessage message = LicenseeServiceTestHelper.getStubMessage();
    PageableList<MasterLicense> pageableList = LicenseeServiceTestHelper.getPageableListStub(20);
    pageableList.setReturnedRecords(LicenseeServiceTestHelper.getStubMasterLicenses());
    List<Integer> offsets = Arrays.asList(2, 4);
    when(masterLicenseWSClientMock.getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class))).thenReturn(pageableList);
    accountServiceMock.mergeByOffsets(offsets, audit, message);
    verify(masterLicenseWSClientMock, times(2)).getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class));
  }

  @Test
  public void testGetUrlQueryParameters() {
    MultiValueMap<String,String> queryParams = accountServiceMock.getUrlQueryParameters(LicenseeServiceTestHelper.OLD_ACCOUNT_ID);
    assertEquals(LicenseeServiceTestHelper.OLD_ACCOUNT_ID, queryParams.get("accountId").get(0));
    assertEquals("header", queryParams.get("fields").get(0));
  }
}
