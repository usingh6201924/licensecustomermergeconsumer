package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class OffsetHelperTest {
  @Test
  public void testPageOffsetList() {
    List<Integer> computedOffsets = OffsetHelper.getRemainingPageOffsets(10, 2);
    List<Integer> expectedOffsets = new ArrayList<Integer>() {{
      add(2);add(4);add(6);add(8);}};
    Assert.assertEquals(expectedOffsets, computedOffsets);
  }

  @Test
  public void testPageOffsetWhenPageSizeIsMoreThanTotalNumberOfRecords() {
    List<Integer> computedOffsets = OffsetHelper.getRemainingPageOffsets(5, 10);
    Assert.assertEquals(Collections.EMPTY_LIST, computedOffsets);
  }
}
