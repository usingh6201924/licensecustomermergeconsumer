package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.transformer.customer;

import com.mathworks.internal.business.license.masterlicensews.types.MasterLicense;
import com.mathworks.internal.business.license.v4.types.CompleteLicenseAdministrator;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class AddAdministratorTransformerTest {

  private List<MasterLicense> masterLicenses;
  private AdminAddRemoveUtil util = new AdminAddRemoveUtil();

  @InjectMocks
  private AddAdministratorTransformer transformer;

  @Before
  public void setUp() {
    masterLicenses = util.createMasterLicenses();
  }

  @After
  public void finalize() {
    masterLicenses = null;
    util =null;
  }

  @Test
  public void testBuildAddAdminsRequest() {
    List<CompleteLicenseAdministrator> addAdmins = transformer.buildAddAdminsRequest(masterLicenses, util.createConsumerMessage());
    Assert.assertEquals(util.NEW_ADMIN_CONTACT_ID, addAdmins.get(0).getContact().getId());
    Assert.assertEquals(1l, addAdmins.size());
  }

  @Test
  public void testBuildAddAdminsRequestEmptyMasters() {
    List<CompleteLicenseAdministrator> addAdmins = transformer.buildAddAdminsRequest(Collections.emptyList(),
        util.createConsumerMessage());
    Assert.assertTrue(addAdmins.isEmpty());
  }

}
