package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common;

import com.mathworks.internal.business.license.masterlicensews.types.Audit;
import com.mathworks.internal.business.license.masterlicensews.types.enums.ReferenceType;
import com.mathworks.internal.business.license.masterlicensews.types.enums.RequestorType;
import static org.junit.Assert.*;
import org.junit.Test;

public class MergeConsumerAuditBuilderTest {
  @Test
  public void testAuditBuilder() {
    String reference = "Test Reference";
    MergeConsumerAuditBuilder builder = new MergeConsumerAuditBuilder();
    builder.setReference(reference);
    Audit audit = builder.getAudit();
    assertEquals(reference, audit.getReferenceId());
    assertEquals("CDS", audit.getRequestorId());
    assertEquals("", audit.getReferenceType());
    assertEquals(RequestorType.BUSINESS_PROCESS.getKey(), audit.getRequestorType());
  }
}
