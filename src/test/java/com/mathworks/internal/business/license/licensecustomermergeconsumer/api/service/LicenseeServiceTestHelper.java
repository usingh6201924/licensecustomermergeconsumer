package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common.MergeConsumerAuditBuilder;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.business.license.masterlicensews.types.*;
import com.mathworks.internal.foundation.support.ws.spring.client.model.PageableList;

import java.util.Arrays;
import java.util.List;

public class LicenseeServiceTestHelper {

  public static final Integer MASTER_LICENSE_ID = 1234;
  public static final String OLD_ACCOUNT_ID = "C_12345";
  public static final String NEW_ACCOUNT_ID = "C_54321";

  public static List<MasterLicense> getStubMasterLicenses() {
    MasterLicense ml = new MasterLicense();
    ml.setId(MASTER_LICENSE_ID);
    Licensee licensee = new Licensee();
    licensee.setId(OLD_ACCOUNT_ID);
    ml.setLicensee(licensee);
    return Arrays.asList(ml);
  }

  public static Audit getStubbedAudit() {
    return new MergeConsumerAuditBuilder().setReference("Reference").getAudit();
  }

  public static MergeConsumerMessage getStubMessage() {
    MergeConsumerMessage message = new MergeConsumerMessage();
    message.setNewId(NEW_ACCOUNT_ID); message.setOldId(OLD_ACCOUNT_ID);
    return message;
  }

  public static PageableList<MasterLicense> getPageableListStub(int numOfRecords) {
    PageableList<MasterLicense> pageableList = new PageableList<>();
    pageableList.setTotalNumberOfRecords(numOfRecords);
    return pageableList;
  }
}
