package com.mathworks.internal.business.license.licensecustomermergeconsumer.consumer;

import org.junit.Test;

import static org.junit.Assert.*;


public class LicenseCustomerMergeConsumerCacheManagerIT {


  private LicenseCustomerMergeConsumerCacheManager cacheManager = new LicenseCustomerMergeConsumerCacheManager();

  @Test
  public void reload() throws Exception {
    assertNotNull(cacheManager.reload());
  }

  @Test
  public void clear() throws Exception {
    assertNotNull(cacheManager.clear());
  }

  @Test
  public void load() throws Exception {
    assertNotNull(cacheManager.load());
  }

}