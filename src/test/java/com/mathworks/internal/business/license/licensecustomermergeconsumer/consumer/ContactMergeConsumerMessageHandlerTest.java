package com.mathworks.internal.business.license.licensecustomermergeconsumer.consumer;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common.LicenseMergeMessageBuilder;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service.customer.ContactService;
import com.mathworks.internal.core.consumerFramework.ConsumerFailException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.core.Message;

@RunWith(MockitoJUnitRunner.class)
public class ContactMergeConsumerMessageHandlerTest {
  private static final String MESSAGE_TEXT = "C1234";
  private static final String ROUTING_KEY = "TestRoutingKey";
  private static final int DELIVERY_TAG = 1234;
  Message message = new Message(MESSAGE_TEXT.getBytes());


  @Mock
  private LicenseMergeMessageBuilder messageBuilder;

  @Mock
  private ContactService contactService;

  @InjectMocks
  private ContactMergeConsumerMessageHandler messageHandler;

  @Test
  public void doHandleMessage() throws Exception {
    MergeConsumerMessage messageStub = new MergeConsumerMessage();
    messageStub.setOldId(MESSAGE_TEXT);
    Mockito.when(messageBuilder.buildMessage(MESSAGE_TEXT)).thenReturn(messageStub);
    Mockito.doNothing().when(contactService).merge(messageStub);
    messageHandler.doHandleMessage(message);
    Mockito.verify(messageBuilder).buildMessage(MESSAGE_TEXT);
    Mockito.verify(contactService, Mockito.times(1)).merge(messageStub);
  }

  @Test (expected = Exception.class)
  public void doHandleMessageWithExceptionRetry() throws Exception {
    Mockito.when(messageBuilder.buildMessage(MESSAGE_TEXT)).thenThrow(new RuntimeException());
    messageHandler.doHandleMessage(message);
  }

  @Test (expected = ConsumerFailException.class)
  public void doHandleMessageWithExceptionFail() throws Exception {
    Mockito.when(messageBuilder.buildMessage(MESSAGE_TEXT)).thenThrow(new ConsumerFailException("Not a valid JSON"));
    messageHandler.doHandleMessage(message);
  }
}
