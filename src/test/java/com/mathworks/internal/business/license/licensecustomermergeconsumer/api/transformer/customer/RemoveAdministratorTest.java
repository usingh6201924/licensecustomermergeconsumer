package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.transformer.customer;

import com.mathworks.internal.business.license.masterlicensews.types.MasterLicense;
import com.mathworks.internal.business.license.v4.types.CompleteLicenseAdministrator;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class RemoveAdministratorTest {

  private List<MasterLicense> masterLicenses;
  private AdminAddRemoveUtil util = new AdminAddRemoveUtil();

  @InjectMocks
  RemoveAdministratorTransformer transformer;

  @Before
  public void setUp() {
    masterLicenses = util.createMasterLicenses();
  }

  @After
  public void finalize() {
    masterLicenses = null;
    util =null;
  }

  @Test
  public void testBuildRemoveAdminsRequest() {
    List<CompleteLicenseAdministrator> removeAdmins = transformer.buildRemoveAdminsRequest(masterLicenses, util.createConsumerMessage());
    Assert.assertEquals(util.ADMIN_ID, removeAdmins.get(0).getId());
    Assert.assertEquals(1l, removeAdmins.size());
  }

  @Test
  public void testBuildRemoveAdminsRequestEmptyMasters() {
    List<CompleteLicenseAdministrator> removeAdmins = transformer.buildRemoveAdminsRequest(Collections.emptyList(),
        util.createConsumerMessage());
    Assert.assertTrue(removeAdmins.isEmpty());
  }
}
