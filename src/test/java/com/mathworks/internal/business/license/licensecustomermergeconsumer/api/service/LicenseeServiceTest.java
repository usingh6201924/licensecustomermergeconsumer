package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.LicenseCustomerMergeReTryException;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service.account.AccountService;
import com.mathworks.internal.business.license.masterlicensews.types.*;
import com.mathworks.internal.foundation.support.ws.spring.client.exception.RestClientFailureException;
import com.mathworks.internal.foundation.support.ws.spring.client.model.PageableList;
import com.mathworks.internal.foundation.support.ws.spring.client.rest.RestClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientResponseException;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LicenseeServiceTest {

  @Mock
  RestClient masterLicenseWSClientMock;
  @InjectMocks
  private AccountService accountServiceMock;



  private MultiValueMap getQueryParamStub() {
    return new LinkedMultiValueMap<>();
  }

  @Test
  public void testGetPageOfMasterLicensesForAccountId() throws Exception {
    PageableList<MasterLicense> pageableList = LicenseeServiceTestHelper.getPageableListStub(200);
    pageableList.setReturnedRecords(LicenseeServiceTestHelper.getStubMasterLicenses());
    when(masterLicenseWSClientMock.getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class))).thenReturn(pageableList);
    PageableList<MasterLicense> result = accountServiceMock.getPageOfMasterLicenses(LicenseeServiceTestHelper.OLD_ACCOUNT_ID, getQueryParamStub());
    verify(masterLicenseWSClientMock).getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class));
    assertEquals(200, result.getTotalNumberOfRecords().intValue());
    assertEquals(1, result.getReturnedRecords().size());
  }

  @Test(expected = LicenseCustomerMergeReTryException.class)
  public void testGetPageOfMasterLicensesForAccountIdException() throws Exception {
    when(masterLicenseWSClientMock.getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class))).thenThrow(RestClientFailureException.class);
    accountServiceMock.getPageOfMasterLicenses(LicenseeServiceTestHelper.OLD_ACCOUNT_ID, getQueryParamStub());
  }

  @Test
  public void testPatchMasterLicensesWithNewAccountId() throws Exception {
    List<MasterLicense> masterLicenses = LicenseeServiceTestHelper.getStubMasterLicenses();
    Audit audit = LicenseeServiceTestHelper.getStubbedAudit();
    MergeConsumerMessage message = LicenseeServiceTestHelper.getStubMessage();
    when(masterLicenseWSClientMock.patch(any(), any(), any())).thenReturn(Collections.EMPTY_LIST);
    accountServiceMock.patchMasterLicensesWithLicensee(audit, masterLicenses, message);
    verify(masterLicenseWSClientMock).patch(any(), any(), any());
  }

  @Test
  public void testPatchMasterLicensesWithLicenseeForEmptyMasterLicenses() throws Exception {
    List<MasterLicense> masterLicenses = new ArrayList<>(); // empty
    Audit audit = LicenseeServiceTestHelper.getStubbedAudit();
    MergeConsumerMessage message = LicenseeServiceTestHelper.getStubMessage();
    accountServiceMock.patchMasterLicensesWithLicensee(audit, masterLicenses, message);
    verify(masterLicenseWSClientMock, never()).patch(any(), any(), any());
  }


  @Test
  public void testPerformPatch() throws Exception {
    MasterLicenseUpdateRequest request = new MasterLicenseUpdateRequest();
    MergeConsumerMessage message = new MergeConsumerMessage();
    message.setOldId(LicenseeServiceTestHelper.OLD_ACCOUNT_ID);message.setNewId(LicenseeServiceTestHelper.NEW_ACCOUNT_ID);

    when(masterLicenseWSClientMock.patch(request, LicenseeService.ENDPOINT + "?fields=header", List.class))
        .thenReturn(Collections.EMPTY_LIST);
    accountServiceMock.performPatch(request, message);
    verify(masterLicenseWSClientMock).patch(request, LicenseeService.ENDPOINT + "?fields=header", List.class);
  }

  @Test(expected = LicenseCustomerMergeReTryException.class)
  public void testPerformPatchHandleException() throws Exception {
    MasterLicenseUpdateRequest request = new MasterLicenseUpdateRequest();
    request.setMasterLicenses(LicenseeServiceTestHelper.getStubMasterLicenses());
    MergeConsumerMessage message = new MergeConsumerMessage();
    message.setOldId(LicenseeServiceTestHelper.OLD_ACCOUNT_ID);message.setNewId(LicenseeServiceTestHelper.NEW_ACCOUNT_ID);

    when(masterLicenseWSClientMock.patch(request, AccountService.ENDPOINT + "?fields=header", List.class))
        .thenThrow(RestClientResponseException.class);
    accountServiceMock.performPatch(request, message);
  }

  @Test
  public void updateMasterLicensesWithNewLicenseeId() {
    List<MasterLicense> masterLicenses = LicenseeServiceTestHelper.getStubMasterLicenses();
    List<MasterLicense> updatedMasterLicenses = accountServiceMock.updateMasterLicensesWithNewLicenseeId(masterLicenses, LicenseeServiceTestHelper.NEW_ACCOUNT_ID);
    assertEquals(1, updatedMasterLicenses.size());
    assertEquals(LicenseeServiceTestHelper.NEW_ACCOUNT_ID, updatedMasterLicenses.get(0).getLicensee().getId());
  }
}
