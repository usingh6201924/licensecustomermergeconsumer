package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service.customer;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service.LicenseeServiceTestHelper;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.transformer.customer.AddAdministratorTransformer;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.transformer.customer.RemoveAdministratorTransformer;
import com.mathworks.internal.business.license.masterlicensews.types.*;
import com.mathworks.internal.business.license.masterlicensews.types.Licensee;
import com.mathworks.internal.business.license.masterlicensews.types.MasterLicense;
import com.mathworks.internal.business.license.v4.types.*;
import com.mathworks.internal.foundation.support.ws.spring.client.model.PageableList;
import com.mathworks.internal.foundation.support.ws.spring.client.rest.RestClient;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.util.MultiValueMap;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceTest {

  private static final String NEW_ADMIN_CONTACT_ID = "C1234";
  private static final String OLD_ADMIN_CONTACT_ID = "C567";
  private static final Integer MASTER_LICENSE_ID = 1234;
  private List<CompleteLicenseAdministrator> administratorsUpdateObjects;

  @Mock
  private RestClient masterLicenseWSClientMock;

  @Mock
  private AddAdministratorTransformer addAdministratorTransformer;

  @Mock
  private RemoveAdministratorTransformer removeAdministratorTransformer;

  @InjectMocks
  private ContactService service;

  @Before
  public void setUp() {
    administratorsUpdateObjects = createAdministratorUpdateObject();
  }

  @After
  public void finalize() {
    administratorsUpdateObjects = null;
  }

  @Test
  public void testMergeWithoutAnyOffsets() throws Exception {
    MergeConsumerMessage message = LicenseeServiceTestHelper.getStubMessage();
    PageableList<MasterLicense> pageableList = LicenseeServiceTestHelper.getPageableListStub(1);
    pageableList.setReturnedRecords(LicenseeServiceTestHelper.getStubMasterLicenses());

    when(masterLicenseWSClientMock.getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class))).thenReturn(pageableList);
    when(masterLicenseWSClientMock.patch(any(), any(), any())).thenReturn(Collections.EMPTY_LIST);
    when(addAdministratorTransformer.buildAddAdminsRequest(any(), any())).thenReturn(administratorsUpdateObjects);
    when(removeAdministratorTransformer.buildRemoveAdminsRequest(any(), any())).thenReturn(administratorsUpdateObjects);
    when(masterLicenseWSClientMock.post((Object) any(),any(),any())).thenReturn(Collections.EMPTY_LIST);
    service.merge(message);
    verify(masterLicenseWSClientMock, times(1)).getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class));
    verify(masterLicenseWSClientMock, times(1)).patch(any(), any(), any());
    verify(masterLicenseWSClientMock, times(2)).post((Object) any(), any(), any()); //one for each add and remove
  }

  private PageableList<MasterLicense> getPageableListStub(int numOfRecords) {
    PageableList<MasterLicense> pageableList = LicenseeServiceTestHelper.getPageableListStub(numOfRecords);
    pageableList.setReturnedRecords(LicenseeServiceTestHelper.getStubMasterLicenses());
    return pageableList;
  }

  @Test
  public void testMergeWithOffsets() throws Exception {
    MergeConsumerMessage message = LicenseeServiceTestHelper.getStubMessage();
    // Returning new PageableList every time getPageableList() is called
    when(masterLicenseWSClientMock.getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class))).thenReturn(
            getPageableListStub(30), getPageableListStub(30));
    when(masterLicenseWSClientMock.patch(any(), any(), any())).thenReturn(Collections.EMPTY_LIST);
    when(addAdministratorTransformer.buildAddAdminsRequest(any(), any())).thenReturn(administratorsUpdateObjects);
    when(removeAdministratorTransformer.buildRemoveAdminsRequest(any(), any())).thenReturn(administratorsUpdateObjects);
    when(masterLicenseWSClientMock.post((Object) any(),any(),any())).thenReturn(Collections.EMPTY_LIST);
    service.merge(message);
    verify(masterLicenseWSClientMock, times(2)).getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class));
    verify(masterLicenseWSClientMock, times(2)).patch(any(), any(), any());
    verify(masterLicenseWSClientMock, times(4)).post((Object) any(), any(), any());//two for each add and remove
  }

  @Test
  public void testMergeByOffsets() throws Exception {
    Audit audit = LicenseeServiceTestHelper.getStubbedAudit();
    MergeConsumerMessage message = LicenseeServiceTestHelper.getStubMessage();
    PageableList<MasterLicense> pageableList = LicenseeServiceTestHelper.getPageableListStub(20);
    pageableList.setReturnedRecords(LicenseeServiceTestHelper.getStubMasterLicenses());
    List<Integer> offsets = Arrays.asList(2, 4);
    when(masterLicenseWSClientMock.getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class))).thenReturn(pageableList);
    service.mergeByOffsets(offsets,audit,message);
    verify(masterLicenseWSClientMock, times(2)).getPageableList(any(String.class),
        any(MultiValueMap.class), any(ParameterizedTypeReference.class),
        any(Integer.class), any(Integer.class));
  }

  @Test
  public void testGetOnlyLicensees() {
    List<MasterLicense> masterLicenses = service.getOnlyLicensees(createMasterLicenses(), OLD_ADMIN_CONTACT_ID);
    Assert.assertEquals(1l, masterLicenses.size());
    Assert.assertEquals(OLD_ADMIN_CONTACT_ID, masterLicenses.get(0).getLicensee().getId());
  }

  @Test
  public void testGetOnlyAdmins() {
    List<MasterLicense> masterLicenses = service.getOnlyAdmins(createMasterLicensesWithAdmins(), OLD_ADMIN_CONTACT_ID);
    Assert.assertEquals(1l, masterLicenses.size());
    Assert.assertEquals(OLD_ADMIN_CONTACT_ID, masterLicenses.get(0).getAdministrators().get(0).getContactId());
  }

  private List<CompleteLicenseAdministrator> createAdministratorUpdateObject() {
    Contact newContact = new Contact();
    newContact.setId(NEW_ADMIN_CONTACT_ID);
    CompleteLicenseAdministrator completeLicenseAdministrator = new CompleteLicenseAdministrator();
    completeLicenseAdministrator.setContact(newContact);
    completeLicenseAdministrator.setOptInRenewalNotifications(Boolean.TRUE);
    completeLicenseAdministrator.setMasterLicenseId(MASTER_LICENSE_ID);
    completeLicenseAdministrator.setOperation(Operation.ADD);
    return Collections.singletonList(completeLicenseAdministrator);
  }

  private List<MasterLicense> createMasterLicenses() {
    MasterLicense masterLicense1 = new MasterLicense();
    MasterLicense masterLicense2 = new MasterLicense();
    Licensee licensee1 = new Licensee();
    licensee1.setId(OLD_ADMIN_CONTACT_ID);
    Licensee licensee2 = new Licensee();
    licensee2.setId(NEW_ADMIN_CONTACT_ID);
    masterLicense1.setLicensee(licensee1);
    masterLicense2.setLicensee(licensee2);
    return Arrays.asList(masterLicense1,masterLicense2);
  }

  private List<MasterLicense> createMasterLicensesWithAdmins() {
    MasterLicense masterLicense1 = new MasterLicense();
    MasterLicense masterLicense2 = new MasterLicense();
    Licensee licensee1 = new Licensee();
    licensee1.setId(OLD_ADMIN_CONTACT_ID);
    Licensee licensee2 = new Licensee();
    licensee2.setId(NEW_ADMIN_CONTACT_ID);
    Administrator administrator1 = new Administrator();
    administrator1.setContactId(NEW_ADMIN_CONTACT_ID);
    Administrator administrator2 = new Administrator();
    administrator2.setContactId(OLD_ADMIN_CONTACT_ID);
    masterLicense1.setLicensee(licensee1);
    masterLicense1.setAdministrators(Arrays.asList(administrator1));
    masterLicense2.setLicensee(licensee2);
    masterLicense2.setAdministrators(Arrays.asList(administrator2));
    return Arrays.asList(masterLicense1,masterLicense2);
  }
}
