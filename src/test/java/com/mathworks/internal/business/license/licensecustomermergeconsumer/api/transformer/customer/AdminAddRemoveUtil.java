package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.transformer.customer;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.business.license.masterlicensews.types.*;

import java.util.Arrays;
import java.util.List;

import static java.util.Collections.singletonList;

public class AdminAddRemoveUtil {
  public static final String NEW_ADMIN_CONTACT_ID = "C123";
  public static final String LICENSEE_CONTACT_ID = "C12";
  public static final String OLD_CONTACT_ID = "C1234";
  public static final Integer ADMIN_ID = 1234;

  public List<MasterLicense> createMasterLicenses() {
    Administrator administrator = new Administrator();
    administrator.setId(ADMIN_ID);
    administrator.setRenewalNotifications(true);
    Licensee licensee = new Licensee();
    licensee.setId(LICENSEE_CONTACT_ID);
    administrator.setContactId(OLD_CONTACT_ID);
    MasterLicense masterLicense = new MasterLicense();
    masterLicense.setId(1);
    masterLicense.setAdministrators(Arrays.asList(administrator));
    masterLicense.setLicensee(licensee);
    return singletonList(masterLicense);
  }

  public MergeConsumerMessage createConsumerMessage() {
    MergeConsumerMessage message = new MergeConsumerMessage();
    message.setEventType("Merge");
    message.setNewId(NEW_ADMIN_CONTACT_ID);
    message.setOldId(OLD_CONTACT_ID);
    return message;
  }

}
