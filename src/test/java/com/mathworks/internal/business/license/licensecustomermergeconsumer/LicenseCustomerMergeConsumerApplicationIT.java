package com.mathworks.internal.business.license.licensecustomermergeconsumer;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.consumer.ContactMergeConsumerConfig;
import com.mathworks.internal.core.consumerFramework.config.rabbitConsumerConfig.ThreadedRabbitConsumerConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LicenseCustomerMergeConsumerApplicationIT {

  @Qualifier("accountmergeconsumerconfig")
  @Autowired
  private ThreadedRabbitConsumerConfig accountMergeConsumerConfig;

  @Qualifier(ContactMergeConsumerConfig.CONTACT_MERGE_CONSUMER_CONFIG_NAME)
  @Autowired
  private ThreadedRabbitConsumerConfig contactmergeconsumerconfig;

  @Test
  public void configurationAndWiring() {
    assertNotNull(accountMergeConsumerConfig.getMessageHandlerName());
    assertNotNull(accountMergeConsumerConfig.getRabbitConfiguration());
    assertNotNull(contactmergeconsumerconfig.getMessageHandlerName());
    assertNotNull(contactmergeconsumerconfig.getRabbitConfiguration());
  }

}
