package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.core.consumerFramework.ConsumerFailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class LicenseMergeMessageBuilder {
  private static final Logger LOG = LoggerFactory.getLogger(LicenseMergeMessageBuilder.class);
  final private ObjectMapper objectMapper;

  @Autowired
  public LicenseMergeMessageBuilder(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  public MergeConsumerMessage buildMessage(String messageText) throws ConsumerFailException {
    try {
      return objectMapper.readValue(messageText, MergeConsumerMessage.class);
    } catch( IOException e ) {
      LOG.error("Error while parsing the message : {} ", messageText);
      // Put the failed message in the Failure Queue for future processing
      throw new ConsumerFailException("Failed to parse message: " + messageText, e);
    }
  }
}
