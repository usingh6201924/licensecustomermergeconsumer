package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.transformer.customer;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.business.license.masterlicensews.types.Administrator;
import com.mathworks.internal.business.license.masterlicensews.types.MasterLicense;
import com.mathworks.internal.business.license.v4.types.CompleteLicenseAdministrator;
import com.mathworks.internal.business.license.v4.types.Operation;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class RemoveAdministratorTransformer {

  public List<CompleteLicenseAdministrator> buildRemoveAdminsRequest(List<MasterLicense> masterLicenses,
                                                                     MergeConsumerMessage message) {
    String oldContactId = message.getOldId();
    Map<Integer, Administrator> masterLicenseAdmins =
        Optional.ofNullable(masterLicenses).orElse(Collections.emptyList()).stream()
        .collect(Collectors.toMap(MasterLicense::getId,
            masterLicense -> findAdministrator(masterLicense.getAdministrators(), oldContactId)));
    masterLicenseAdmins.values().removeIf(Objects::isNull);
    return Optional.of(masterLicenseAdmins.keySet()).orElse(Collections.emptySet()).stream()
        .map(masterLicenseId -> createRemoveAdminRequest(masterLicenseAdmins.get(masterLicenseId), masterLicenseId))
        .collect(Collectors.toList());
  }

  protected CompleteLicenseAdministrator createRemoveAdminRequest(Administrator administrator,
                                                                  int masterLicenseId) {
    CompleteLicenseAdministrator completeLicenseAdministrator = new CompleteLicenseAdministrator();
    completeLicenseAdministrator.setMasterLicenseId(masterLicenseId);
    completeLicenseAdministrator.setId(administrator.getId());
    completeLicenseAdministrator.setOperation(Operation.REMOVE);
    return  completeLicenseAdministrator;
  }

  protected Administrator findAdministrator(List<Administrator> administrators, String oldContactId) {
    return Optional.ofNullable(administrators).orElse(Collections.emptyList()).stream()
        .filter(administrator -> oldContactId.equals(administrator.getContactId()))
        .findAny()
        .orElse(null);
  }

}
