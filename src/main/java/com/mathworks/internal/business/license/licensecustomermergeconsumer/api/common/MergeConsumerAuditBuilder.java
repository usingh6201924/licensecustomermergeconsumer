package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common;

import com.mathworks.internal.business.license.masterlicensews.types.Audit;
import com.mathworks.internal.business.license.masterlicensews.types.enums.ReferenceType;
import com.mathworks.internal.business.license.masterlicensews.types.enums.RequestorType;

public class MergeConsumerAuditBuilder {
  private String reference;
  private static final String REQUESTOR_ID = "CDS";
  private static final String EMPTY_REFERENCE_TYPE = ""; // Reference Type should be empty string. If null or not set to a value, the UI will render the CALLER_ID in the History tab.

  public MergeConsumerAuditBuilder setReference(String reference) {
    this.reference = reference;
    return this;
  }

  public Audit getAudit() {
    Audit audit = new Audit();
    audit.setReferenceType(EMPTY_REFERENCE_TYPE);
    audit.setReferenceId(reference);
    audit.setRequestorType(RequestorType.BUSINESS_PROCESS.getKey());
    audit.setRequestorId(REQUESTOR_ID);
    return audit;
  }
}
