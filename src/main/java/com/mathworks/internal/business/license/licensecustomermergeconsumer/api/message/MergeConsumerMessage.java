package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.util.Assert;
import java.io.Serializable;

public class MergeConsumerMessage implements Serializable {
  private String eventType;
  private String newId;
  private String oldId;
  private String sourceTable;

  @JsonCreator
  public MergeConsumerMessage(
      @JsonProperty(value = "eventType") String eventType,
      @JsonProperty(value = "sourceId", required = true) String newId,
      @JsonProperty(value = "sourceDupId", required = true)  String oldId,
      @JsonProperty(value = "sourceTable") String sourceTable
  ) {
    Assert.hasLength(oldId, "sourceDupId is null or Empty");
    Assert.hasLength(newId, "sourceId is null or Empty");
    this.eventType = eventType;
    this.newId = newId;
    this.oldId = oldId;
    this.sourceTable = sourceTable;
  }

  public MergeConsumerMessage() {}

  public String getEventType() {
    return eventType;
  }

  public void setEventType(String eventType) {
    this.eventType = eventType;
  }

  public String getNewId() {
    return newId;
  }

  public void setNewId(String newId) {
    this.newId = newId;
  }

  public String getOldId() {
    return oldId;
  }

  public void setOldId(String oldId) {
    this.oldId = oldId;
  }

  public String getSourceTable() {
    return sourceTable;
  }

  public void setSourceTable(String sourceTable) {
    this.sourceTable = sourceTable;
  }
}
