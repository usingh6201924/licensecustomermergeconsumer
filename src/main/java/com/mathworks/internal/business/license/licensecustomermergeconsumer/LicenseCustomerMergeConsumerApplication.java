package com.mathworks.internal.business.license.licensecustomermergeconsumer;

import com.mathworks.internal.core.consumerFramework.springSupport.ConsumerSpringConfig;
import com.mathworks.internal.core.logging.support.LogTrackingConfig;
import com.mathworks.internal.core.logging.support.logback.LogControlConfig;
import com.mathworks.internal.core.healthmonitorapp.HealthCheckConfiguration;
import com.mathworks.internal.core.cache.CacheAdminConfiguration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({ConsumerSpringConfig.class, LogTrackingConfig.class,LogControlConfig.class, HealthCheckConfiguration.class, CacheAdminConfiguration.class})
public class LicenseCustomerMergeConsumerApplication {
  public static void main(String[] args) {
    SpringApplication.run(LicenseCustomerMergeConsumerApplication.class, args);
  }
}
