package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service.customer;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.LicenseCustomerMergeReTryException;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.client.ClientConfiguration;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common.MergeConsumerAuditBuilder;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common.OffsetHelper;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service.LicenseeService;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.transformer.customer.AddAdministratorTransformer;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.transformer.customer.RemoveAdministratorTransformer;
import com.mathworks.internal.business.license.masterlicensews.types.*;
import com.mathworks.internal.business.license.masterlicensews.types.enums.LicenseeType;
import com.mathworks.internal.business.license.v4.types.CompleteLicenseAdministrator;
import com.mathworks.internal.foundation.support.ws.spring.client.model.PageableList;
import com.mathworks.internal.foundation.support.ws.spring.client.rest.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ContactService extends LicenseeService {
  private static final Logger LOG = LoggerFactory.getLogger(ContactService.class);
  private static final Integer PAGE_LIMIT = 25;
  private static final String LICENSE_DATA_WS_ENDPOINT = "administrators/";
  private final RestClient licenseDataWSClient;
  private final AddAdministratorTransformer addAdministratorTransformer;
  private final RemoveAdministratorTransformer removeAdministratorTransformer;

  public ContactService(
      @Autowired
      @Qualifier(ClientConfiguration.MASTER_LICENSE_WS_CLIENT_BEAN) RestClient masterLicenseWSClient,
      @Autowired
      @Qualifier(ClientConfiguration.LICENSE_DATA_WS_CLIENT_BEAN)
      RestClient licenseDataWSClient, AddAdministratorTransformer addAdministratorTransformer, RemoveAdministratorTransformer removeAdministratorTransformer) {
    super(masterLicenseWSClient, PAGE_LIMIT, LicenseeType.CONTACT);
    this.licenseDataWSClient = licenseDataWSClient;
    this.addAdministratorTransformer = addAdministratorTransformer;
    this.removeAdministratorTransformer = removeAdministratorTransformer;
  }

  /**
   * The ContactMergeConsumer handles the scenario where as result of a deduplication or other CDS operation
   * A licensee id (Contact) changed on a master- This is handled through the MLWS Patch call
   * An Administrator id (Contact) changed on a master - This is handled through a post call to LicenseDataWs
   * Please see- https://jira.mathworks.com/browse/LICACT-2652
   * @param message
   */
  public void merge(MergeConsumerMessage message) {
    Audit audit = new MergeConsumerAuditBuilder().setReference("CDS Contact Merge").getAudit();
    String oldContactId= message.getOldId();
    MultiValueMap<String, String> queryParam = getUrlQueryParameters(oldContactId);
    PageableList<MasterLicense> firstPageOfMasterLicenses = getPageOfMasterLicenses(message.getOldId(), queryParam);// First Page
    List<MasterLicense> updateLicensee = getOnlyLicensees(firstPageOfMasterLicenses.getReturnedRecords(),oldContactId);
    patchMasterLicensesWithLicensee(audit, updateLicensee, message);
    List<MasterLicense> updateAdmins = getOnlyAdmins(firstPageOfMasterLicenses.getReturnedRecords(), oldContactId);
    postAddAdminWithNewContactId(updateAdmins, message);
    postRemoveAdmin(updateAdmins, message);
    List<Integer> offsets = OffsetHelper.getRemainingPageOffsets(firstPageOfMasterLicenses.getTotalNumberOfRecords(), PAGE_LIMIT);
    mergeByOffsets(offsets, audit, message);
  }

  protected void mergeByOffsets(List<Integer> offsets, Audit audit, MergeConsumerMessage message) {
    MultiValueMap<String, String> queryParam = getUrlQueryParameters(message.getOldId());
    String oldContactId= message.getOldId();
    offsets.forEach(offset -> {
      PageableList<MasterLicense> masterLicenses = getPageOfMasterLicenses(oldContactId, queryParam);
      List<MasterLicense> updateLicensee = getOnlyLicensees(masterLicenses.getReturnedRecords(),oldContactId);
      patchMasterLicensesWithLicensee(audit, updateLicensee, message);
      List<MasterLicense> updateAdmins = getOnlyAdmins(masterLicenses.getReturnedRecords(),oldContactId);
      postAddAdminWithNewContactId(updateAdmins, message);
      postRemoveAdmin(updateAdmins, message);
    });
  }

  protected List<MasterLicense> getOnlyLicensees(List<MasterLicense> masterLicenses, String oldContactId) {
    return Optional.ofNullable(masterLicenses).orElse(Collections.emptyList()).stream()
        .filter(masterLicense -> (oldContactId).equals(masterLicense.getLicensee().getId()))
        .collect(Collectors.toList());
  }

  protected List<MasterLicense> getOnlyAdmins(List<MasterLicense> masterLicenses, String oldContactId) {
    return Optional.ofNullable(masterLicenses).orElse(Collections.emptyList()).stream()
        .filter(masterLicense -> matchOldAdmin(masterLicense.getAdministrators(),oldContactId))
        .collect(Collectors.toList());
  }

  private boolean matchOldAdmin(List<Administrator> administrators, String oldContactId) {
    return Optional.ofNullable(administrators).orElse(Collections.emptyList()).stream()
        .anyMatch(administrator -> (oldContactId).equals(administrator.getContactId()));
  }

  protected void postAddAdminWithNewContactId (List<MasterLicense> masterLicenses,
                                               MergeConsumerMessage message) {
    List<CompleteLicenseAdministrator> addAdministrators =
        addAdministratorTransformer.buildAddAdminsRequest(masterLicenses, message);
    //Add administrator through LicenseDataWs currently supports only 1 request per payload
    addAdministrators.forEach(
        administrator-> postAdministrator(Collections.singletonList(administrator), message));
  }

  protected void postRemoveAdmin(List<MasterLicense> masterLicenses,
                                 MergeConsumerMessage message) {
    List<CompleteLicenseAdministrator> removeAdministrators =
        removeAdministratorTransformer.buildRemoveAdminsRequest(masterLicenses, message);
    //Remove administrator through LicenseDataWs currently supports only 1 request per payload
      removeAdministrators.forEach(
        administrator-> postAdministrator(Collections.singletonList(administrator), message));
  }

  private void postAdministrator(List<CompleteLicenseAdministrator> administrator,
                                   MergeConsumerMessage message) {
    /* The SpringRestClient library does not support an option to pass the Query parameters as MultiValueMap for the
    post call. That is the reason why the query parameter is appended to the Endpoint.*/
    try {
      licenseDataWSClient.post(administrator,  LICENSE_DATA_WS_ENDPOINT, List.class);
    }catch(Exception e){
      String errorMessage = generateLoggingErrorMessage(administrator, message);
      LOG.error(errorMessage, e);
      throw new LicenseCustomerMergeReTryException(errorMessage, e);
    }
  }

  protected MultiValueMap<String, String> getUrlQueryParameters(String licenseeId) {
    MultiValueMap<String, String> urlQueryParameters = new LinkedMultiValueMap<>();
    urlQueryParameters.add("contactId", licenseeId);
    urlQueryParameters.add("fields", "header,administrators");
    return urlQueryParameters;
  }

  private String generateLoggingErrorMessage (List<CompleteLicenseAdministrator> administrator,
                                              MergeConsumerMessage message) {
    Integer masterLicenseId = administrator.get(0).getMasterLicenseId();
    String masterLicensesDetails = MessageFormat.format("Error while performing post call for" +
        " master licenses {0}.", masterLicenseId);
    String postRequestType = "Post Request Type is Admins " + administrator.get(0).getOperation().value();
    String contactDetails = MessageFormat.format(" Old Contact ID : {0}, New Contact ID: {1}.", message.getOldId(), message.getNewId());
    String errorMessage = masterLicensesDetails + postRequestType + contactDetails;
    return errorMessage;
  }
}
