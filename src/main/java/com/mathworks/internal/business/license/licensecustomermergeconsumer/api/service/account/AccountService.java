package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service.account;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.client.ClientConfiguration;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common.MergeConsumerAuditBuilder;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common.OffsetHelper;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service.LicenseeService;
import com.mathworks.internal.business.license.masterlicensews.types.Audit;
import com.mathworks.internal.business.license.masterlicensews.types.MasterLicense;
import com.mathworks.internal.business.license.masterlicensews.types.enums.LicenseeType;
import com.mathworks.internal.foundation.support.ws.spring.client.model.PageableList;
import com.mathworks.internal.foundation.support.ws.spring.client.rest.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;

@Component
public class AccountService extends LicenseeService {
  private static final Logger LOG = LoggerFactory.getLogger(AccountService.class);
  private static final Integer PAGE_LIMIT = 25;

  public AccountService(
      @Autowired
      @Qualifier(ClientConfiguration.MASTER_LICENSE_WS_CLIENT_BEAN) RestClient masterLicenseWSClient) {
    super(masterLicenseWSClient, PAGE_LIMIT, LicenseeType.ACCOUNT);
  }

  public void merge(MergeConsumerMessage message) {
    Audit audit = new MergeConsumerAuditBuilder().setReference("CDS Account Merge").getAudit();
    MultiValueMap<String, String> queryParams = getUrlQueryParameters(message.getOldId());
    PageableList<MasterLicense> firstPageOfMasterLicenses = getPageOfMasterLicenses(message.getOldId(), queryParams); // First Page
    patchMasterLicensesWithLicensee(audit, firstPageOfMasterLicenses.getReturnedRecords(), message);
    List<Integer> offsets = OffsetHelper.getRemainingPageOffsets(firstPageOfMasterLicenses.getTotalNumberOfRecords(), PAGE_LIMIT);
    mergeByOffsets(offsets, audit, message);
  }

  /*
   * Once we fetch a page of master licenses for Old Account ID, we update the Account ID to the new Account ID and save
   * the changes. This will cause the get call to return 1 page less after every iteration. Since we calculated the
   * offsets after the first get call, the # of offsets is equal to # of pages to fetch.
   */
  protected void mergeByOffsets(List<Integer> offsets, Audit audit, MergeConsumerMessage message) {
    MultiValueMap<String, String> queryParams = getUrlQueryParameters(message.getOldId());
    offsets.forEach(offset -> {
      PageableList<MasterLicense> masterLicenses = getPageOfMasterLicenses(message.getOldId(), queryParams);
      patchMasterLicensesWithLicensee(audit, masterLicenses.getReturnedRecords(), message);
    });
  }

  protected MultiValueMap<String, String> getUrlQueryParameters(String accountId) {
    MultiValueMap<String, String> urlQueryParameters = new LinkedMultiValueMap<>();
    urlQueryParameters.add("accountId", accountId);
    urlQueryParameters.add("fields", "header");
    return urlQueryParameters;
  }
}
