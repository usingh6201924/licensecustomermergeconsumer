package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.transformer.customer;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.business.license.masterlicensews.types.Administrator;
import com.mathworks.internal.business.license.masterlicensews.types.MasterLicense;
import com.mathworks.internal.business.license.v4.types.*;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class AddAdministratorTransformer {

  public List<CompleteLicenseAdministrator> buildAddAdminsRequest(List<MasterLicense> masterLicenses,
                                                                  MergeConsumerMessage message) {
    String newContactId = message.getNewId();
    String oldContactId = message.getOldId();
    Map<Integer, Boolean> masterLicenseIds = Optional.ofNullable(masterLicenses).orElse(Collections.emptyList()).stream()
        .collect(Collectors.toMap(MasterLicense::getId,
            masterLicense -> findOptInRenewalNotification(masterLicense.getAdministrators(), oldContactId)));
    return masterLicenseIds.keySet().stream()
        .map(masterLicenseId -> createAddAdminRequest(masterLicenseId,masterLicenseIds.get(masterLicenseId), newContactId))
        .collect(Collectors.toList());
  }

  protected CompleteLicenseAdministrator createAddAdminRequest(int masterLicenseId, boolean optInRenewalNotification,
                                                               String newId) {
    Contact newContact = new Contact();
    newContact.setId(newId);
    CompleteLicenseAdministrator completeLicenseAdministrator = new CompleteLicenseAdministrator();
    completeLicenseAdministrator.setContact(newContact);
    completeLicenseAdministrator.setOptInRenewalNotifications(optInRenewalNotification);
    completeLicenseAdministrator.setMasterLicenseId(masterLicenseId);
    completeLicenseAdministrator.setOperation(Operation.ADD);
    return  completeLicenseAdministrator;
  }

  protected boolean findOptInRenewalNotification(List<Administrator> administrators, String oldContactId) {
    Administrator oldAdministrator = Optional.ofNullable(administrators).orElse(Collections.emptyList()).stream()
        .filter(administrator -> oldContactId.equals(administrator.getContactId()))
        .findAny()
        .orElse(null);
    if (oldAdministrator !=null) {
      return oldAdministrator.getRenewalNotifications();
    }
    return true;
  }

}
