package com.mathworks.internal.business.license.licensecustomermergeconsumer.healthcheck;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.client.ClientConfiguration;
import com.mathworks.internal.core.healthcheck.alive.RestAliveCheck;
import com.mathworks.internal.core.healthcheck.alive.RestPingCheck;
import com.mathworks.internal.core.healthmonitor.AbstractHealthCheck;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class HealthCheckConfig {
  @Bean
  public AbstractHealthCheck masterLicenseWsAliveCheck(){
    return new RestAliveCheck(ClientConfiguration.MASTER_LICENSE_WS_NAMED_RESOURCE_KEY, aliveRestTemplate());
  }

  @Bean
  public RestPingCheck licenseDataWsHealthCheck() {
    return new RestPingCheck(ClientConfiguration.LICENSE_DATA_WS_NAMED_RESOURCE_KEY, aliveRestTemplate());
  }

  public RestTemplate aliveRestTemplate(){
    return new RestTemplateBuilder().build();
  }
}
