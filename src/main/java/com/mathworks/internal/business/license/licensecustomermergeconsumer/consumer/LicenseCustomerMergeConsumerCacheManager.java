package com.mathworks.internal.business.license.licensecustomermergeconsumer.consumer;

import com.mathworks.internal.core.consumerFramework.ConsumerCacheManager;
import org.springframework.stereotype.Component;

@Component(LicenseCustomerMergeConsumerCacheManager.BEAN_NAME)
public class LicenseCustomerMergeConsumerCacheManager implements ConsumerCacheManager {
  public static final String BEAN_NAME = "LicenseCustomerMergeConsumerCacheManager";
  @Override
  public String reload() {
    // do the work to reload your cache here
    return "cache reload not implemented yet";
  }

  @Override
  public String clear() {
    // do the work to clear your cache here
    return "cache clear not implemented yet";
  }

  @Override
  public String load() {
    // do the work to load your cache here
    return "cache load not implemented yet";
  }
}
