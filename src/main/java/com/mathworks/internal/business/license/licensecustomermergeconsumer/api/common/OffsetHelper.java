package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common;

import java.util.ArrayList;
import java.util.List;

public class OffsetHelper {
  /**
   * Returns a list of offsets which can be used to make multiple calls to a service.
   * This method is to be called after fetching first page in order to get the Total Number of Records
   * @param totalNumberOfRecords
   * @param pageSize
   * @return
   */
  public static List<Integer> getRemainingPageOffsets(int totalNumberOfRecords,
                                                      Integer pageSize) {
    Double limit = new Double(pageSize);
    int numberOfPages = new Double(Math.ceil(totalNumberOfRecords/limit)).intValue();
    List<Integer> offsets = new ArrayList<>();
    // Since the first page (page 0) is already fetched, we start from 1
    for(int pageNumber = 1; pageNumber < numberOfPages; pageNumber ++) {
      offsets.add(pageSize.intValue() * pageNumber);
    }
    return offsets;
  }
}
