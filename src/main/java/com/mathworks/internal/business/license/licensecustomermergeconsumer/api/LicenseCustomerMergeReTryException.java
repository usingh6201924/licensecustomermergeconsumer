package com.mathworks.internal.business.license.licensecustomermergeconsumer.api;

/**
 * On throwing this exception, the message will be put back in the Queue and the message will be re-tried
 */
public class LicenseCustomerMergeReTryException extends RuntimeException {

  public LicenseCustomerMergeReTryException(String msg) {
    super(msg);
  }

  public LicenseCustomerMergeReTryException(String msg, Throwable t) {
    super(msg, t);
  }

  public LicenseCustomerMergeReTryException(Throwable t) {
    super(t);
  }
}
