package com.mathworks.internal.business.license.licensecustomermergeconsumer.consumer;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common.LicenseMergeMessageBuilder;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service.customer.ContactService;
import com.mathworks.internal.core.consumerFramework.AbstractRabbitConsumerMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(ContactMergeConsumerMessageHandler.CONTACT_MERGE_CONSUMER_BEAN_NAME)
@Scope("prototype")
public class ContactMergeConsumerMessageHandler extends AbstractRabbitConsumerMessageHandler {

  public static final String CONTACT_MERGE_CONSUMER_BEAN_NAME = "contactMergeConsumerMessageHandlerBean";
  private static final Logger LOG = LoggerFactory.getLogger(ContactMergeConsumerMessageHandler.class);
  private static final String LOG_RECEIVED_MESSAGE = "routing key: %s, deliveryTag: %s, message received: %s, headers: %s";

  private final LicenseMergeMessageBuilder messageBuilder;
  private final ContactService contactService;

  public ContactMergeConsumerMessageHandler(LicenseMergeMessageBuilder messageBuilder, ContactService contactService) {
    this.messageBuilder = messageBuilder;
    this.contactService = contactService;
  }

  @Override
  public void doHandleMessage(Message receivedMessage) throws Exception {
    String messageText = new String(receivedMessage.getBody());
    LOG.info(String.format(LOG_RECEIVED_MESSAGE, receivedMessage.getMessageProperties().getReceivedRoutingKey(),
        receivedMessage.getMessageProperties().getDeliveryTag(), messageText,
        receivedMessage.getMessageProperties().getHeaders()));
    MergeConsumerMessage message = messageBuilder.buildMessage(messageText);
    contactService.merge(message);
  }

}
