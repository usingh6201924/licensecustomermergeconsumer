package com.mathworks.internal.business.license.licensecustomermergeconsumer.consumer;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.common.LicenseMergeMessageBuilder;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service.account.AccountService;
import com.mathworks.internal.core.consumerFramework.AbstractRabbitConsumerMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(AccountMergeConsumerMessageHandler.ACCOUNT_MERGE_CONSUMER_BEAN_NAME)
@Scope("prototype")
public class AccountMergeConsumerMessageHandler extends AbstractRabbitConsumerMessageHandler {
  public static final String ACCOUNT_MERGE_CONSUMER_BEAN_NAME = "accountMergeConsumerMessageHandlerBean";
  private static final Logger LOG = LoggerFactory.getLogger(AccountMergeConsumerMessageHandler.class);
  private static final String LOG_RECEIVED_MESSAGE = "routing key: %s, deliveryTag: %s, message received: %s, headers: %s";

  private final LicenseMergeMessageBuilder messageBuilder;
  private final AccountService accountService;

  @Autowired
  public AccountMergeConsumerMessageHandler(
      LicenseMergeMessageBuilder messageBuilder, AccountService accountService) {
    this.messageBuilder = messageBuilder;
    this.accountService = accountService;
  }

  @Override
  public void doHandleMessage(Message receivedMessage) throws Exception {
    String messageText = new String(receivedMessage.getBody());
    LOG.info(String.format(LOG_RECEIVED_MESSAGE, receivedMessage.getMessageProperties().getReceivedRoutingKey(),
        receivedMessage.getMessageProperties().getDeliveryTag(), messageText,
        receivedMessage.getMessageProperties().getHeaders()));
    MergeConsumerMessage message = messageBuilder.buildMessage(messageText);
    accountService.merge(message);
  }

}
