package com.mathworks.internal.business.license.licensecustomermergeconsumer.consumer;

import com.mathworks.internal.core.consumerFramework.config.rabbitConsumerConfig.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class AccountMergeConsumerConfig {

  private static final String CONSUMER_GROUP_NAME = "LICENSE_CUSTOMER_MERGE_CONSUMER";
  private static final String CONSUMER_NAME = "AccountMergeConsumer";
  private static final String CONSUMER_AMQP_NAMED_RESOURCE = "CoreModels";
  private static final String CONSUMER_RABBIT_EXCHANGE_NAME = "CDS";
  private static final String CONSUMER_RABBIT_QUEUE_NAME = "AccountMergeLicenseV2";
  private static final String CONSUMER_RABBIT_QUEUE_BINDING = "account.merged";

  @Bean("accountmergeconsumerconfig")
  ThreadedRabbitConsumerConfig accountMergeConsumerConfig() {
    return new ThreadedRabbitConsumerConfigBuilder(CONSUMER_NAME, AccountMergeConsumerMessageHandler.ACCOUNT_MERGE_CONSUMER_BEAN_NAME, rabbitConfiguration())
        .groupName(CONSUMER_GROUP_NAME)
        .cacheManagerName(LicenseCustomerMergeConsumerCacheManager.BEAN_NAME)
        .build();
  }

  private RabbitConfiguration rabbitConfiguration() {
    return new RabbitConfiguration(
        CONSUMER_AMQP_NAMED_RESOURCE,
        CONSUMER_RABBIT_EXCHANGE_NAME,
        CONSUMER_RABBIT_QUEUE_NAME,
        CONSUMER_RABBIT_QUEUE_BINDING);
    //
    // if you want non-default behavior for your rabbit queues and exchanges, you can set additional properties
    // on the RabbitConfiguration instance.  A couple examples are listed below
    //
    // rabbitConfiguration.setRabbitExchangeType(RabbitContext.ExchangeType.fanout);  // default is topic
    // rabbitConfiguration.setRabbitQueueAutoDelete(true);   // default is false
  }
}