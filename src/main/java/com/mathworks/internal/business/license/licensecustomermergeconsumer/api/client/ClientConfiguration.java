package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.client;

import com.mathworks.internal.foundation.support.ws.spring.client.rest.RestClient;
import com.mathworks.internal.foundation.support.ws.spring.client.rest.RestClientConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClientConfiguration {
  public static final String MASTER_LICENSE_WS_NAMED_RESOURCE_KEY = "MasterLicenseWebServiceV1";
  public static final String LICENSE_DATA_WS_NAMED_RESOURCE_KEY = "LicenseDataServiceV4";

  public static final String MASTER_LICENSE_WS_CLIENT_BEAN = "MasterLicenseWSClientBean";
  public static final String LICENSE_DATA_WS_CLIENT_BEAN = "LicenseDataWSClientBean";
  public static final String CALLER_ID = "LCMC"; // License Customer Merge Consumer

  @Bean
  public RestClientConfiguration restClientConfiguration() {
    RestClientConfiguration config = new RestClientConfiguration();
    config.setCallerId(CALLER_ID);
    return config;
  }

  @Bean(name = MASTER_LICENSE_WS_CLIENT_BEAN)
  public RestClient restContactClient() {
    return new RestClient(restClientConfiguration(), MASTER_LICENSE_WS_NAMED_RESOURCE_KEY);
  }

  @Bean(name = LICENSE_DATA_WS_CLIENT_BEAN)
  public RestClient restAdminClient() {
    return new RestClient(restClientConfiguration(), LICENSE_DATA_WS_NAMED_RESOURCE_KEY);
  }
}
