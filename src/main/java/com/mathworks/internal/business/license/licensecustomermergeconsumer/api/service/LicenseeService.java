package com.mathworks.internal.business.license.licensecustomermergeconsumer.api.service;

import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.LicenseCustomerMergeReTryException;
import com.mathworks.internal.business.license.licensecustomermergeconsumer.api.message.MergeConsumerMessage;
import com.mathworks.internal.business.license.masterlicensews.types.*;
import com.mathworks.internal.business.license.masterlicensews.types.enums.LicenseeType;
import com.mathworks.internal.foundation.support.ws.spring.client.exception.RestClientFailureException;
import com.mathworks.internal.foundation.support.ws.spring.client.model.PageableList;
import com.mathworks.internal.foundation.support.ws.spring.client.rest.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.util.MultiValueMap;

import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

public abstract class LicenseeService {
  private static final Logger LOG = LoggerFactory.getLogger(LicenseeService.class);
  protected static final String ENDPOINT = "masterLicenses/";
  private final RestClient masterLicenseWSClient;
  private Integer pageLimit;
  private LicenseeType licenseeType;
  public LicenseeService(RestClient masterLicenseWSClient, Integer pageLimit, LicenseeType licenseeType) {
    this.masterLicenseWSClient = masterLicenseWSClient;
    this.pageLimit = pageLimit;
    this.licenseeType = licenseeType;
  }

  protected PageableList<MasterLicense> getPageOfMasterLicenses(String licenseeId,
                                                                MultiValueMap<String, String> queryParam) {
    try {
      return masterLicenseWSClient.getPageableList(
          ENDPOINT, queryParam,
          new ParameterizedTypeReference<List<MasterLicense>>() {
          }, 0, pageLimit);
    }catch(RestClientFailureException e) {
      LOG.error("Error while fetching Master License for " + licenseeType.getKey() + ", licenseeId:" + licenseeId, e);
      throw new LicenseCustomerMergeReTryException("Error while fetching Master License for " + licenseeType.getKey() + " : " + licenseeId, e);
    }
  }

  protected void patchMasterLicensesWithLicensee(Audit audit, List<MasterLicense> masterLicenses, MergeConsumerMessage message) {
    if(masterLicenses != null && masterLicenses.size() > 0) {
      List<MasterLicense> updatedMasterLicenses = updateMasterLicensesWithNewLicenseeId(masterLicenses, message.getNewId());
      MasterLicenseUpdateRequest updateRequest = buildUpdateRequest(updatedMasterLicenses, audit);
      performPatch(updateRequest, message);
    }
  }

  protected MasterLicenseUpdateRequest buildUpdateRequest(List<MasterLicense> masterLicenses, Audit audit) {
    MasterLicenseUpdateRequest updateRequest = new MasterLicenseUpdateRequest();
    updateRequest.setAudit(audit);
    updateRequest.setMasterLicenses(masterLicenses);
    return updateRequest;
  }

  protected void performPatch(MasterLicenseUpdateRequest request, MergeConsumerMessage message) {
    try {
      /* The SpringRestClient library does not support an option to pass the Query parameters as MultiValueMap for the
      patch call. That is the reason why the query parameter is appended to the Endpoint.*/
      masterLicenseWSClient.patch(request, ENDPOINT + "?fields=header", List.class);
    }catch(Exception e){
      List<Integer> masterLicenseIds = request.getMasterLicenses().stream().map(MasterLicense::getId).collect(Collectors.toList());
      String masterLicensesDetails = MessageFormat.format("Error while performing patch for master licenses {0}.",masterLicenseIds);
      String contactDetails = MessageFormat.format(" Old Contact ID : {0}, New Contact ID: {1}.", message.getOldId(), message.getNewId());
      String errorMessage = masterLicensesDetails + contactDetails;
      LOG.error(errorMessage, e);
      throw new LicenseCustomerMergeReTryException(errorMessage, e);
    }
  }

  protected List<MasterLicense> updateMasterLicensesWithNewLicenseeId(List<MasterLicense> masterLicenses,
                                                                      String licenseeId) {
    return masterLicenses.stream().map(m -> {
      m.getLicensee().setId(licenseeId);
      m.getLicensee().setType(this.licenseeType.getKey());
      return m;
    }).collect(Collectors.toList());
  }
}
